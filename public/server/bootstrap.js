require('ignore-styles')
require('isomorphic-fetch')
require('@babel/polyfill')
require('@babel/register')({
  ignore: [/(node_modules)/],
  presets: ['@babel/preset-env', '@babel/preset-react'],
  plugins: [
    [
      'babel-plugin-webpack-alias',
      { config: './config/webpack.resolve.config.js' }
    ],
    '@babel/plugin-proposal-class-properties'
  ]
})
require('./index')
