// Libraries
import React from 'react'
import 'moment/locale/fr'
import moment from 'moment'
import createStore from 'Redux'
import { Provider } from 'react-redux'
import { renderToString } from 'react-dom/server'
import { handleServerRequest } from '@react-navigation/web'

// Ant Design
import { LocaleProvider } from 'antd'
import frFR from 'antd/lib/locale-provider/fr_FR'

import { AppNavigator } from 'Navigation/NavigationRouter'

// I18n
import 'I18n'

const path = require('path')
const fs = require('fs')

// Changement de la locale de moment
moment.locale('fr')

// Création du store
const store = createStore()

export default (req, res, next) => {
  const filePath = path.resolve(__dirname, '../..', 'index.html')

  const { path: uri, query } = req

  const { navigation } = handleServerRequest(AppNavigator.router, uri, query)

  const html = renderToString(
    <LocaleProvider locale={frFR}>
      <Provider store={store}>
        <AppNavigator navigation={navigation} />
      </Provider>
    </LocaleProvider>
  )

  fs.readFile(filePath, 'utf8', (err, htmlData) => {
    if (err) {
      console.error('err', err)
      return res.status(404).end()
    }
    return res.send(
      htmlData.replace('<div id="root"></div>', `<div id="root">${html}</div>`)
    )
  })
}
