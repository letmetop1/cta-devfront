import express from 'express'
// import serverRenderer from './middleware/renderer'

require('dotenv').config()

const PORT = process.env.PORT || 3000
const path = require('path')

const app = express()
const router = express.Router()

router.use(
  '*/static',
  express.static(path.resolve(__dirname, '..', 'static'), {
    maxAge: '30d'
  })
)

// Server side rendering
// router.use('/*', serverRenderer)

// Client side rendering
router.use('/*', (req, res) => {
  res.status(200).sendFile(path.resolve(__dirname, '..', 'index.html'))
})

app.use(router)

app.listen(PORT, error => {
  if (error) {
    return console.log('something bad happened', error)
  }
  console.log('listening on ' + PORT + '...')
})
