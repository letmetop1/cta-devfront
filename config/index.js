const {
  override,
  fixBabelImports,
  addLessLoader,
  addWebpackResolve
} = require('customize-cra')

const path = require('path')

const paths = require('react-scripts/config/paths')

// Configuration Webpack de resolve alias
const { resolve } = require('./webpack.resolve.config.js')

// Configuration des chemins d'entrée
paths.appIndexJs = path.resolve(paths.appSrc, 'index.js')

module.exports = override(
  addWebpackResolve(resolve),
  fixBabelImports('antd', {
    libraryName: 'antd',
    style: true
  }),
  fixBabelImports('ant-design-pro', {
    libraryName: 'ant-design-pro',
    libraryDirectory: 'lib',
    style: true,
    camel2DashComponentName: false
  }),
  addLessLoader({
    modifyVars: {
      '@primary-color': process.env.REACT_APP_MAIN_COLOR,
      '@secondary-color': process.env.REACT_APP_SECOND_COLOR
    },
    javascriptEnabled: true
  })
)
