// Libraries
import I18n from 'i18next'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import React, { Component } from 'react'
import { Layout, Menu, Icon, Avatar } from 'antd'
import { Link } from '@react-navigation/web'
import {
  filter,
  includes,
  map,
  isNil,
  has,
  forEach,
  defaultTo,
  get,
  toNumber,
  isEmpty,
  cloneDeep
} from 'lodash'

// Components
import Images from 'Images'

const { Sider } = Layout

class CustomSider extends Component {
  static propTypes = {
    theme: PropTypes.string,
    links: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    collapsible: PropTypes.bool
  }

  static defaultProps = {
    theme: 'light',
    collapsible: false,
    projects: [],
    links: {
      bar: {
        subMenu: 'CTA',
        title: 'pages.cta.title',
        screens: [
          {
            routeName: 'Home',
            title: 'pages.home.title',
            matchScreens: ['Home']
          },
          {
            routeName: 'UserData',
            title: 'pages.userData.title',
            matchScreens: ['UserData']
          },
          {
            routeName: 'Guild',
            title: 'pages.guild.title',
            matchScreens: ['Guild']
          }
        ]
      }
    }
  }

  constructor(props) {
    super(props)

    this.state = {
      collapsed: false
    }
  }

  /**
   * Prélève tous les écrans dans une liste d'écrans groupés
   */
  _flattenLinks = (links, subMenu) => {
    const flatten = []

    forEach(links, link => {
      if (has(link, 'subMenu')) {
        flatten.push(...this._flattenLinks(link.screens, link.subMenu))
      } else {
        if (!isNil(subMenu)) {
          link.subMenu = subMenu
        }

        flatten.push(link)
      }
    })

    return flatten
  }

  _handleCollapse = collapsed => {
    this.setState({ collapsed })
  }

  _renderMenuLink = ({
    title,
    routeName,
    image,
    params,
    subMenu,
    key = routeName || subMenu,
    icon = false,
    screens,
    ...props
  }) => {
    return !isNil(screens) ? (
      <Menu.SubMenu
        key={key}
        title={
          <span>
            {icon && <Icon type={icon} />}
            <span>{defaultTo(I18n.t(title), '')}</span>
          </span>
        }
      >
        {map(screens, this._renderMenuLink)}
      </Menu.SubMenu>
    ) : (
      <Menu.Item key={key}>
        <Link
          routeName={defaultTo(routeName, '')}
          params={defaultTo(params, {})}
        >
          {/* Image ou icon */}
          {image ? (
            <Avatar shape='square' src={image} icon={icon} />
          ) : (
            icon && <Icon type={icon} />
          )}
          <span>{defaultTo(I18n.t(title), '')}</span>
        </Link>
      </Menu.Item>
    )
  }

  _renderMenu = () => {
    const { links } = this.props

    const currentScreen = {
      id: get(this.props, 'currentScreen.params.id'),
      routeName: get(this.props, 'currentScreen.routeName')
    }

    const flattenLinks = this._flattenLinks(cloneDeep(links))

    // Récupération des actifs en fonction du matchScreens
    const associatedScreens = filter(
      flattenLinks,
      ({ matchScreens, matchIDs }) => {
        let isMatching

        // Passage en nombre pour être sûr que la comparaison soit bonne
        matchIDs = map(matchIDs, toNumber)

        // Page avec match ID précis
        if (!isEmpty(matchIDs)) {
          isMatching =
            includes(matchScreens, get(currentScreen, 'routeName')) &&
            includes(matchIDs, toNumber(get(currentScreen, 'id')))
        } else {
          isMatching = includes(matchScreens, get(currentScreen, 'routeName'))
        }

        return isMatching
      }
    )

    const selectedKeys = map(associatedScreens, route =>
      defaultTo(get(route, 'key'), get(route, 'routeName'))
    )

    const openKeys = defaultTo(
      get(this.state, 'openKeys'),
      map(associatedScreens, 'subMenu')
    )

    return (
      <Menu
        defaultSelectedKeys={selectedKeys}
        selectedKeys={selectedKeys}
        onOpenChange={openKeys => this.setState({ openKeys })}
        defaultOpenKeys={openKeys}
        openKeys={openKeys}
        mode='inline'
      >
        {map(links, this._renderMenuLink)}
      </Menu>
    )
  }

  render() {
    const { collapsed } = this.state
    const { theme, collapsible } = this.props

    const logo = Images.icon

    return (
      <Sider
        theme={theme}
        collapsible={collapsible}
        collapsed={collapsed}
        onCollapse={this._handleCollapse}
      >
        {/* Logo de l'application */}
        <Link routeName='Dashboard'>
          <div className='ant-layout-sider-logo'>
            <img
              className='ant-layout-sider-logo-image'
              src={logo}
              alt='logo'
            />
          </div>
        </Link>

        {/* Menu */}
        {this._renderMenu()}
      </Sider>
    )
  }
}

const mapStateToProps = state => {
  const defaultProps = get(CustomSider, 'defaultProps', {})

  return {
    projects: defaultTo(get(state, 'project.items'), defaultProps.projects)
  }
}

const mapDispatchToProps = dispatch => ({})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomSider)
