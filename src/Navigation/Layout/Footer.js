import React, { Component } from 'react'

// Libraries
import { Layout } from 'antd'
// import PropTypes from 'prop-types'

const { Footer } = Layout

class CustomFooter extends Component {
  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {
    return (
      <Footer>
        Made with <span style={{ color: 'red' }}>❤</span> by{' '}
        <a href='https://ubidreams.fr'>Ubidreams</a>
      </Footer>
    )
  }
}

export default CustomFooter
