// Libraries
import { createNavigator, StackRouter } from '@react-navigation/core'
import I18n from 'i18next'

// Utils
import { prepareStack } from 'Navigation/Utils'

// Layout
import { AuthenticatedLayout } from 'Navigation/Layout'

// Views
import Home from 'Views/Home'
import Profile from 'Views/Profile'
import UserData from 'Views/UserData'

// Stacks
export const Stack = StackRouter(
  prepareStack({
    Home: {
      screen: Home,
      path: 'home',
      navigationOptions: {
        title: I18n.t('pages.home.titleCustom')
      }
    },
    UserData: {
      screen: UserData,
      path: 'userData',
      navigationOptions: {
        title: I18n.t('pages.home.titleCustom')
      }
    },
    Profile: {
      screen: Profile,
      path: 'profile',
      navigationOptions: {
        title: I18n.t('pages.profile.title')
      }
    }
  }),
  {
    initialRouteName: 'Home'
  }
)

// Navigators
const navigationConfig = {}
const Navigator = createNavigator(AuthenticatedLayout, Stack, navigationConfig)

export default Navigator
