import { createResource } from 'redux-rest-resource'
import { baseURL } from './'

export const { types, actions, rootReducer } = createResource({
  name: 'profile',
  url: `${baseURL}`,
  credentials: 'include',
  actions: {
    get: { method: 'GET', gerundName: 'getting', url: './profile' }
  }
})
