import { createResource } from 'redux-rest-resource'
import { baseURL } from './'

export const { types, actions, rootReducer } = createResource({
  name: 'hero',
  pluralName: 'heroes',
  url: `${baseURL}/api`,
  credentials: 'include',
  actions: {
    fetch: { method: 'GET', gerundName: 'fetching', url: './heroes' },
    get: {
      method: 'GET',
      gerundName: 'getting',
      url: './box/:id'
    },
    create: { method: 'POST', gerundName: 'creating', url: './box' },
    update: {
      method: 'PUT',
      gerundName: 'updating',
      url: './box/:id',
      assignResponse: true
    },
    archive: {
      method: 'PATCH',
      gerundName: 'deleting',
      url: './box/:id/archive'
    },
    archiveMany: {
      isArray: true,
      alias: 'archive',
      method: 'POST',
      gerundName: 'archiving',
      url: './box/archive'
    }
  }
})
