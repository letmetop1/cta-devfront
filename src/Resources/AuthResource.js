import { createResource } from 'redux-rest-resource'
import { baseURL } from './'

/*
  Ceci est un exemple de resource mais pas utilisée
 */
export const { types, actions, rootReducer } = createResource({
  name: 'auth',
  url: `${baseURL}`,
  actions: {
    login: { method: 'POST', gerundName: 'loggingIn', url: './login' },
    forgotten: {
      method: 'POST',
      gerundName: 'askingForgotten',
      url: './admin/password-forgotten'
    },
    firstLogin: {
      method: 'POST',
      gerundName: 'firstLoggingIn',
      url: './admin/first-login'
    },
    checkTempCode: {
      method: 'PUT',
      gerundName: 'checkingTempoCode',
      url: './admin/check-temp-code'
    },
    createPassword: {
      method: 'POST',
      gerundName: 'addingPassword',
      url: './admin/password-change'
    },
    logout: { method: 'POST', gerundName: 'loggingOut', url: './logout' },
    set: {
      isPure: true,
      reduce: (state, { context: credentials }) => ({
        ...state,
        item: credentials
      })
    },
    // Vidage du reducer
    reset: {
      isPure: true,
      reduce: state => ({ ...state, item: null })
    }
  },
  credentials: 'include'
})
