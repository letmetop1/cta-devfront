# WebSocket Middleware
Created by Clément MOINE
WebSocket Middleware is a middleware that allows multiples connection to websocket, registering status, handling messages through Sagas and Redux.

# How to install

  - Import the folder WebSocket in your project (containing: `Redux.js`, `Sagas.js`, `Middleware.js`)
  - Link the middleware to Redux in your `CreateStore.js`:
```js
import WebSocketMiddleware from 'WebSocket/Middleware'

...

middleware.push(WebSocketMiddleware)
```
  - Link the Redux to your Redux `index.js`:
```js
...

// Resources
import { reducer as webSocketReducer } from 'WebSocket/Redux'

...

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  let rootReducer = combineReducers({
    websocket: webSocketReducer
  })

  ...

```
  - Link the Redux to your Redux `index.js`:
```js
/* ------------- Types ------------- */

import { WebSocketTypes } from 'WebSocket/Redux'

/* ------------- Sagas ------------- */

import { handleMessage, disconnected, connected, watchNotificationChannel } from 'WebSocket/Sagas'

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield all([
    // WebSocket
    takeLatest(StartupTypes.STARTUP_DONE, watchNotificationChannel),
    takeLatest(WebSocketTypes.WS_MESSAGE_RECEIVED, handleMessage),
    takeLatest(WebSocketTypes.WS_DISCONNECTED, disconnected),
    takeLatest(WebSocketTypes.WS_CONNECTED, connected)
  ])
}
```
