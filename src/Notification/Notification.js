// Libraries
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import React, { Component } from 'react'
import Notification from 'react-web-notification'
import { get, head, omit, find, defaultTo } from 'lodash'

// Actions
import NotificationActions from 'Notification/Redux'

// Services
import NavigationServices from 'Services/NavigationServices'

class Notifications extends Component {
  static propTypes = {
    notifications: PropTypes.array,
    actions: PropTypes.object
  }

  static defaultProps = {
    notifications: []
  }

  constructor(props) {
    super(props)

    this.state = {
      ignore: true
    }
  }

  /**
   * Récupération de la dernière notification disponible
   */
  _getLastNotification = () => {
    const { notifications = [] } = this.props

    return head(notifications)
  }

  /**
   * Autorisation donnée
   */
  handlePermissionGranted = () => {
    this.setState({ ignore: false })
  }

  /**
   * Autorisation bloquée
   */
  handlePermissionDenied = () => {
    this.setState({ ignore: true })
  }

  /**
   * Notifications non-supportés
   */
  handleNotSupported = () => {
    this.setState({ ignore: true })
  }

  /**
   * Click sur une notification
   */
  handleNotificationOnClick = (e, tag) => {
    const { notifications } = this.props

    const notification = find(notifications, { tag })

    NavigationServices.navigate('Booking', {
      id: get(notification, 'booking.id')
    })
  }

  /**
   * Erreur sur les notifications
   */
  handleNotificationOnError = (e, tag) => {}

  /**
   * Fermeture de la notification
   */
  handleNotificationOnClose = (e, tag) => {
    // Suppression de la liste des notifications à traiter
    this.props.actions.removeNotification(tag)
  }

  /**
   * Ouverture de la notification
   */
  handleNotificationOnShow = (e, tag) => {}

  render() {
    const notification = this._getLastNotification()
    const { ignore } = this.state

    const title = defaultTo(get(notification, 'title'), '')
    const options = omit(notification, 'title')

    return (
      <Notification
        ignore={ignore || notification === ''}
        notSupported={this.handleNotSupported}
        onPermissionGranted={this.handlePermissionGranted}
        onPermissionDenied={this.handlePermissionDenied}
        onShow={this.handleNotificationOnShow}
        onClick={this.handleNotificationOnClick}
        onClose={this.handleNotificationOnClose}
        onError={this.handleNotificationOnError}
        timeout={10000}
        title={title}
        options={options}
      />
    )
  }
}

const mapStateToProps = state => {
  const defaultProps = get(Notifications, 'defaultProps', {})

  return {
    notifications: defaultTo(
      get(state, 'notification.items'),
      defaultProps.notifications
    )
  }
}

const mapDispatchToProps = dispatch => ({
  actions: {
    removeNotification: tag =>
      dispatch(NotificationActions.removeNotification(tag))
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Notifications)
