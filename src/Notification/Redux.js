// Libraries
import I18n from 'i18next'
import Immutable from 'seamless-immutable'
import { map, defaultTo, get, filter, now } from 'lodash'
import { createReducer, createActions } from 'reduxsauce'

// Images
import Images from 'Images'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  createNotifications: ['notifications'],
  clearNotifications: null,

  removeNotification: ['tag']
})

export const WebSocketTypes = Types

export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  items: []
})

/* ------------- Reducers ------------- */

// Notification reçue
export const create = ({ items, ...state }, { notifications }) => {
  notifications = map(notifications, (notification, id) => ({
    ...notification,
    tag: defaultTo(get(notification, 'tag'), `notification_${id}_${now()}`),
    title: defaultTo(
      get(notification, 'title'),
      I18n.t('notification.default.title')
    ),
    body: defaultTo(
      get(notification, 'body'),
      I18n.t('notification.default.body')
    ),
    icon: defaultTo(get(notification, 'icon'), Images.icon)
  }))

  return { ...state, items: [...items, ...notifications] }
}

// Notification lue
export const remove = ({ items: notifications, ...state }, { tag }) => {
  notifications = filter(
    notifications,
    notification => notification.tag !== tag
  )

  return { ...state, items: notifications }
}

// Réinitialisation de la state
export const reset = (state, action) => {
  return INITIAL_STATE
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CREATE_NOTIFICATIONS]: create,
  [Types.CLEAR_NOTIFICATIONS]: reset,

  [Types.REMOVE_NOTIFICATION]: remove
})
