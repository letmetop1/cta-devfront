import Immutable from 'seamless-immutable'
import { reactotronRedux as reduxPlugin } from 'reactotron-redux'
import sagaPlugin from 'reactotron-redux-saga'
import tronsauce from 'reactotron-apisauce'

// Client side rendering / Server Side
const isDOM = typeof window !== 'undefined'

if (process.env.NODE_ENV !== 'production' && isDOM) {
  const Reactotron = require('reactotron-react-js').default

  // https://github.com/infinitered/reactotron for more options!
  Reactotron.configure({
    name: 'Electron'
  })
    .use(reduxPlugin({ onRestore: Immutable }))
    .use(sagaPlugin())
    .use(tronsauce())
    .connect()

  // Let's clear Reactotron on every time we load the app
  Reactotron.clear()

  // Totally hacky, but this allows you to not both importing reactotron-react-native
  // on every file.  This is just DEV mode, so no big deal.
  console.tron = Reactotron
}
