import * as ReactotronConfig from './ReactotronConfig'
import ReduxPersist from './ReduxPersist'

export default ReactotronConfig
export { ReactotronConfig, ReduxPersist }
