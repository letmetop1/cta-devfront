// @flow

// Libraries
import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'

// Sagas
import rootSaga from 'Sagas/'

// Store
import configureStore from 'Redux/CreateStore'

// Reducer
import { reducer as startupReducer } from 'Redux/StartupRedux'

// Resources
import { navReducer } from 'Navigation/NavigationRouter'
import { rootReducer as apiReducer } from 'Resources/ApiResource'
import { rootReducer as langReducer } from 'Resources/LangResource'
import { rootReducer as authReducer } from 'Resources/AuthResource'
import { rootReducer as profileReducer } from 'Resources/ProfileResource'
import { rootReducer as fileReducer } from 'Resources/FileResource'
import { reducer as webSocketReducer } from 'WebSocket/Redux'
import { rootReducer as heroReducer } from 'Resources/HeroResource'
import { rootReducer as artefactReducer } from 'Resources/ArtefactResource'

// Config
import { ReduxPersist } from 'Config'

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  let rootReducer = combineReducers({
    startup: startupReducer,
    lang: langReducer,
    nav: navReducer,
    api: apiReducer,
    auth: authReducer,
    profile: profileReducer,
    file: fileReducer,
    websocket: webSocketReducer,
    heroes: heroReducer,
    artefacts: artefactReducer
  })

  /* ------------- Config persistant store ------------- */
  if (ReduxPersist.active) {
    rootReducer = persistReducer(ReduxPersist.storeConfig, rootReducer)
  }

  return configureStore(rootReducer, rootSaga)
}
