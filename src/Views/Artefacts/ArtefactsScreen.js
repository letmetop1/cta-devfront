// Libraries
import I18n from 'i18next'
import PropTypes from 'prop-types'
import { message, Modal, Rate } from 'antd'
import { connect } from 'react-redux'
import { get, defaultTo } from 'lodash'
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'

// Ressources
import { actions as artefactActions } from 'Resources/ArtefactResource'

// Components
import { TableLayout } from 'Components'
import { Drawer } from './Components'

// Modal
import { ManageBox } from './Components/ManageModal'

// Styles
import './Styles/ArtefactsScreen.less'

const { confirm } = Modal

class Artefacts extends Component {
  static propTypes = {
    actions: PropTypes.object,
    artefacts: PropTypes.array,
    isGathering: PropTypes.bool,
    tables: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    buttons: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    navigation: PropTypes.shape({
      navigate: PropTypes.func.isRequired,
      getParam: PropTypes.func.isRequired
    })
  }

  static defaultProps = {
    artefacts: [],
    isGathering: false,
    isSubmitting: false,
    buttons: {
      create: {
        label: I18n.t('common.create'),
        icon: 'plus'
      }
    },
    tables: {
      artefacts: {
        multiDelete: {},
        locale: {
          emptyText: I18n.t('pages.artefacts.table.empty')
        },
        pagination: {
          position: 'both',
          showSizeChanger: true
        },
        rowKey: 'id',
        columns: {
          name: {
            title: I18n.t('pages.data.components.tabs.artefacts.table.name'),
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) =>
              defaultTo(get(a, 'name'), '').localeCompare(
                defaultTo(get(b, 'name'), '')
              )
          },
          lvl: {
            title: I18n.t('pages.data.components.tabs.artefacts.table.lvl'),
            dataIndex: 'lvl',
            key: 'lvl',
            sorter: (a, b) =>
              defaultTo(get(a, 'lvl'), 0) - defaultTo(get(b, 'lvl'), 0)
          },
          stars: {
            title: I18n.t('pages.data.components.tabs.artefacts.table.stars'),
            dataIndex: 'nb_etoile',
            key: 'nb_etoile',
            render: artefacts => {
              let stars = get(artefacts, 'nb_etoile')
              return <Rate disabled defaultValue={stars} />
            },
            sorter: (a, b) =>
              defaultTo(get(a, 'nb_etoile'), 0) -
              defaultTo(get(b, 'nb_etoile'), 0)
          }
        }
      }
    }
  }

  constructor(props) {
    super(props)

    this.state = {
      openedModal: '',
      drawerOpened: false
    }
  }

  componentDidMount = () => {
    // Récupération des données des boîtiers
    this.fetchArtefactsData()
  }

  /**
   * Récupération des données des boîtiers
   */
  fetchArtefactsData = () => {
    console.tron.log(this.props.actions)
    this.props.actions.fetchArtefacts().catch(() => {
      message.error(I18n.t('api.errors.box.get'))
    })
  }

  /**
   * Archivage de boîtiers
   */
  archiveBoxes = box => {
    this.props.actions
      .archiveBoxes(box)
      .then(() => {
        message.success(I18n.t(`api.success.box.archiveMany`))
      })
      .catch(() => {
        message.error(I18n.t(`api.errors.box.archiveMany`))
      })
  }

  /**
   * Demande de création d'un boîtier
   */
  askCreateBox = () => {
    this.setState({
      openedModal: 'createBox'
    })
  }

  /**
   * Demande d'archivage de boîtiers
   */
  askArchiveBoxes = box => {
    confirm({
      title: I18n.t('pages.boxes.components.modal.archiveBoxes.title'),
      content: I18n.t('pages.boxes.components.modal.archiveBoxes.content'),
      okText: I18n.t('common.archive'),
      okType: 'danger',
      cancelText: I18n.t('common.cancel'),
      onOk: () => {
        this.archiveBoxes(box)
      },
      maskClosable: true
    })
  }

  /**
   * Fermeture de toutes les modales
   */
  handleCloseModal = () => {
    this.setState({ openedModal: '' })
  }

  /**
   * Fermeture de la modale après validation
   */
  handleBoxCreated = () => {
    this.setState({ openedModal: '' })
  }

  /**
   * Clic sur une ligne de la liste globale
   */
  handleBoxClick = box => {
    this.setState({ box, drawerOpened: true })
  }

  /**
   * Fermeture du drawer
   */
  handleCloseDrawer = box => {
    this.setState({ box: {}, drawerOpened: false })
  }

  render() {
    const { tables, buttons, isGathering, artefacts } = this.props
    const { drawerOpened, box, openedModal } = this.state

    // Ajout des données au tableau
    tables.artefacts.rows = artefacts

    // Ajout des actions au clic sur un ligne du tableau
    tables.artefacts.onRow = (box, id) => ({
      onClick: () => this.handleBoxClick(box, id)
    })

    // Ajout du multiple archive sur le tableau
    tables.artefacts.multiDelete.onClick = this.askArchiveBoxes
    tables.artefacts.multiDelete.title = count =>
      I18n.t('components.tableLayout.archiveMany', { count })

    // Ajout des actions des boutons
    // buttons.create.action = this.askCreateBox

    return (
      <main className='screen box'>
        {/* Affichage des boîtiers */}

        <TableLayout
          elementName='artefact'
          elementKind='male'
          elementConsonant
          buttons={buttons}
          loading={isGathering}
          tables={tables}
        />

        {/* Creation d'un boîtier */}
        <ManageBox
          visible={openedModal === 'createBox'}
          onCancel={this.handleCloseModal}
          onOk={this.handleBoxCreated}
        />

        {/* Affichage d'un boîtier */}
        <Drawer
          onClose={this.handleCloseDrawer}
          id={get(box, 'id')}
          visible={drawerOpened}
        />
      </main>
    )
  }
}

const mapStateToProps = state => {
  const defaultProps = get(Artefacts, 'defaultProps', {})

  return {
    artefacts: defaultTo(get(state, 'artefacts.items'), defaultProps.artefacts)
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ ...artefactActions }, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Artefacts)
