// Libraries
import I18n from 'i18next'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { Form, Input, message, Select } from 'antd'
import { get, has, map, defaultTo, cloneDeep, groupBy, isNil } from 'lodash'

// Components
import { Container } from 'Components/Modals'

// Resources
import { actions as heroActions } from 'Resources/HeroResource'

// Styles
import './Styles/Modal.less'

// Helper
import { createFormFields } from 'Helpers'

const { Item: FormItem } = Form
const { Option, OptGroup } = Select

class ManageBoxModal extends Component {
  static propTypes = {
    // Contrôle d'état de la modale
    okText: PropTypes.string,
    title: PropTypes.string,
    visible: PropTypes.bool,

    // Contenu de la modale
    box: PropTypes.object,
    form: PropTypes.object,
    boxAreas: PropTypes.array,

    // Appels API
    actions: PropTypes.object,

    // Fonctions de callback
    onOk: PropTypes.func,
    onCancel: PropTypes.func
  }

  static defaultProps = {
    visible: false,
    formData: {},
    onOk: () => {},
    onCancel: () => {},
    box: {},
    boxAreas: [],
    okText: I18n.t('pages.boxes.components.modal.createBox.okText'),
    title: I18n.t('pages.boxes.components.modal.createBox.title')
  }

  constructor(props) {
    super(props)

    // États initiaux
    this.state = {
      visible: get(props, 'visible'),
      loading: false
    }

    // Modale ouverte en mode création ?
    this.isCreating = !has(props.box, 'id')
  }
  componentDidMount = () => {
    // this.fetchBoxArea()
  }

  componentDidUpdate(prevProps) {
    const { box, visible } = this.props

    if (prevProps.box !== box) {
      // Modale ouverte en mode création ?
      this.isCreating = !has(box, 'id')
    }

    if (prevProps.visible !== visible) {
      // Changement de visibilité de la modale
      this.changeModalVisibility(visible)
    }
  }

  /**
   * Changement de visibilité de la modale
   */
  changeModalVisibility = visible => {
    this.setState({ visible })
  }

  handleCancel = e => {
    const { onCancel, form } = this.props

    this.setState(
      {
        visible: false
      },
      () => {
        onCancel(e)
        form.resetFields()
      }
    )
  }

  createBox = box => {
    const { form, onOk } = this.props

    this.props.actions
      .createBox(box)
      .then(({ body: box }) => {
        message.success(I18n.t('api.success.box.create'))
        onOk(box)

        form.resetFields()
      })
      .catch(err => {
        // Problème lors de la création
        if (err.status === 400) {
          message.error(I18n.t('api.errors.box.create'))
        }
      })
      .finally(() => {
        this.setState({
          loading: false
        })
      })
  }

  updateBox = box => {
    const { form, onOk } = this.props

    this.props.actions
      .updateBox({ ...box, boxArea: defaultTo(get(box, 'boxArea'), null) })
      .then(({ body: box }) => {
        message.success(I18n.t('api.success.box.update'))

        // Callbacks
        onOk(box)
        form.resetFields()

        // Récupération des box libres
        this.fetchBoxArea()
      })
      .catch(err => {
        // Problème lors de la création
        if (err) {
          message.error(I18n.t('api.errors.box.update'))
        }
      })
      .finally(() => {
        this.setState({
          loading: false
        })
      })
  }

  fetchBoxArea = () => {
    this.props.actions.fetchBoxAreas().catch(() => {
      message.error(I18n.t('api.errors.box.getBoxArea'))
    })
  }

  handleSubmit = e => {
    e.preventDefault()
    const {
      form,
      box: { id }
    } = this.props

    // Validation du formulaire
    form.validateFieldsAndScroll((error, box) => {
      if (!error) {
        this.setState({ loading: true }, () => {
          this.isCreating
            ? this.createBox({ ...box })
            : this.updateBox({ id, ...box })
        })
      }
    })
  }

  render() {
    const { visible = false, loading = false } = this.state
    const { form, title, okText, boxAreas } = this.props
    const { getFieldDecorator } = form

    // Changement de titre (création ou modification)
    const updatedTitle = !this.isCreating
      ? I18n.t('pages.boxes.components.modal.updateBox.title')
      : title
    const updatedOkText = !this.isCreating
      ? I18n.t('pages.boxes.components.modal.updateBox.okText')
      : okText

    return (
      <Container
        title={updatedTitle}
        visible={visible}
        loading={loading}
        onOk={this.handleSubmit}
        okText={updatedOkText}
        onCancel={this.handleCancel}
        className={'modal createBox'}
      >
        <Form onSubmit={this.handleSubmit}>
          {/* Numéro de série */}
          <FormItem label={I18n.t(`fields.serialNumber.title`)}>
            {getFieldDecorator('serialNumber', {
              rules: [
                {
                  required: true,
                  message: I18n.t(`fields.serialNumber.requiredMessage`)
                }
              ]
            })(
              <Input placeholder={I18n.t(`fields.serialNumber.placeholder`)} />
            )}
          </FormItem>

          {/* zone associée que si update */}

          {!this.isCreating && (
            <FormItem label={I18n.t(`fields.boxArea.title`)}>
              {getFieldDecorator('boxArea', {})(
                <Select
                  allowClear
                  placeholder={I18n.t(`fields.boxArea.placeholder`)}
                >
                  {map(groupBy(boxAreas, 'type'), (areas, type) => (
                    <OptGroup
                      key={type}
                      label={I18n.t(
                        `pages.boxes.table.columns.associated.type.${type}`
                      )}
                    >
                      {map(areas, area => (
                        <Option key={area.id} value={area.id}>
                          {area.name}
                        </Option>
                      ))}
                    </OptGroup>
                  ))}
                </Select>
              )}
            </FormItem>
          )}

          {/* identifiant matériel Lora à masquer si pas Ubiadmin */}
          <FormItem label={I18n.t(`fields.devEUI.title`)}>
            {getFieldDecorator('devEUI', {
              rules: [
                {
                  required: true,
                  message: I18n.t(`fields.devEUI.requiredMessage`)
                }
              ]
            })(<Input placeholder={I18n.t(`fields.devEUI.placeholder`)} />)}
          </FormItem>
        </Form>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  const defaultProps = get(ManageBoxModal, 'defaultProps', {})

  let availableBoxAreas = []

  // Ajout de la zone box actuellement affectée
  if (!isNil(get(state, 'box.item.boxArea'))) {
    availableBoxAreas.push(get(state, 'box.item.boxArea'))
  }

  // Ajout des zones box disponibles
  availableBoxAreas = [
    ...availableBoxAreas,
    ...defaultTo(get(state, 'boxArea.items'), defaultProps.boxAreas)
  ]

  return {
    boxAreas: availableBoxAreas
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      ...heroActions
    },
    dispatch
  )
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  Form.create({
    mapPropsToFields(props) {
      const defaultProps = get(ManageBoxModal, 'defaultProps', {})
      const box = cloneDeep(defaultTo(get(props, 'box'), defaultProps.box))
      box.boxArea = get(box, 'boxArea.id')

      const isCreating = !has(props.box, 'id')

      if (!isCreating) {
        box.linkedProjects = map(box.linkedProjects, 'id')
      }

      return createFormFields(Form, box)
    }
  })(ManageBoxModal)
)
