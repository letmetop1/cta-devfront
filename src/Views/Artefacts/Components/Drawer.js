// Libraries
import I18n from 'i18next'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { get, defaultTo, isNil } from 'lodash'
import { Drawer, Button, message, Modal } from 'antd'

// Styles
import './Styles/HeroesDrawer.less'

// Ressources
import { actions as heroActions } from 'Resources/HeroResource'

// Modal
import { ManageBox } from './ManageModal'

const { confirm } = Modal

class HeroesDrawer extends Component {
  static propTypes = {
    // Données de la vue
    id: PropTypes.number,
    box: PropTypes.object,

    // Contrôle de l'affichage
    visible: PropTypes.bool,

    // Fonctions
    onClose: PropTypes.func,
    actions: PropTypes.object
  }

  static defaultProps = {
    // Données de la vue
    box: {}
  }

  constructor(props) {
    super(props)

    // États initiaux
    this.state = {
      openedModal: '',
      box: {},
      visible: get(props, 'visible')
    }
  }

  componentDidUpdate = oldProps => {
    const { id, visible } = this.props

    // Changement de cadenas ouvert depuis la liste
    if (!isNil(id) && oldProps.id !== id) {
      this.getBoxData()
    }

    // Changement de visibilité du panneau
    if (oldProps.visible !== visible) {
      this.changeModalVisibility(visible)
    }
  }

  /**
   * Changement de visibilité du panneau
   */
  changeModalVisibility = visible => {
    this.setState({ visible })
  }

  /**
   * Fermeture du panneau
   */
  handleClose = () => {
    const { onClose } = this.props

    this.setState({ visible: false }, () => setTimeout(onClose, 300))
  }

  /**
   * Récupération des données du cadenas
   */
  getBoxData = () => {
    const { id } = this.props

    this.props.actions
      .getBox(id)
      .catch(() => message.error(I18n.t('api.errors.box.get')))
  }

  /**
   * Rendu d'une ligne de détails formatée (ex: Label: Valeur)
   */
  renderDescriptionItem = (name, value) => (
    <p className='box-details-description-item'>
      <span className='box-details-description-item-name'>
        {I18n.t(`pages.boxes.components.drawer.description.${name}`)}
      </span>
      <span className='box-details-description-item-value'>
        {defaultTo(value, get(this.props, `box.${name}`))}
      </span>
    </p>
  )

  /**
   * Mise en archive du boîtier
   */
  archiveBox = () => {
    const { box } = this.props

    confirm({
      title: I18n.t('pages.boxes.components.modal.archiveBox.title'),
      content: I18n.t('pages.boxes.components.modal.archiveBox.content'),
      okText: I18n.t('common.archive'),
      cancelText: I18n.t('common.cancel'),
      okType: 'danger',
      onOk: () => {
        this.props.actions
          .archiveBox({
            id: get(box, 'id')
          })
          .then(() => {
            message.success(I18n.t(`api.success.box.archive`))
          })
          .catch(() => {
            message.error(I18n.t(`api.errors.box.archive`))
          })
      }
    })
  }

  /**
   * Ouverture de la modale de mise à jour
   */
  askUpdateBox = () => {
    this.setState({ openedModal: 'updateBox' })
  }

  /**
   * Fermeture de toutes les modales
   */
  handleCloseModal = () => {
    this.setState({ openedModal: '' })
  }

  /**
   * Fermeture de la modale après validation
   */
  handleBoxUpdate = () => {
    this.setState({ openedModal: '' })
  }

  render() {
    const { box } = this.props
    const { visible, openedModal } = this.state

    return (
      <Drawer
        width={640}
        placement='right'
        onClose={this.handleClose}
        visible={visible}
        className='box-details'
      >
        {/* Titre */}
        <h2>{I18n.t('pages.boxes.components.drawer.title')}</h2>

        {/* Informations principales */}
        <main className='box-details-description'>
          {/* Informations détaillés  */}
          <section className='box-details-description-group'>
            <h3>{I18n.t('pages.boxes.components.drawer.sections.details')}</h3>

            {/* Numéro de série */}
            {this.renderDescriptionItem('serialNumber')}

            {/* Lieu associé au boîtier */}
            {this.renderDescriptionItem('boxArea.name')}

            {/* identifiant Lora */}
            {this.renderDescriptionItem('devEUI')}
          </section>
        </main>

        {/* Boutons d'actions */}
        <footer className='box-details-footer'>
          <Button onClick={this.askUpdateBox} type='primary'>
            {I18n.t('common.update')}
          </Button>

          <div className='buttons'>
            <Button onClick={this.archiveBox} type='danger'>
              {I18n.t('common.archive')}
            </Button>
          </div>
        </footer>

        {/* Modale de mise à jour */}
        <ManageBox
          visible={openedModal === 'updateBox'}
          onCancel={this.handleCloseModal}
          onOk={this.handleBoxUpdate}
          box={box}
        />
      </Drawer>
    )
  }
}

const mapStateToProps = state => {
  const defaultProps = get(HeroesDrawer, 'defaultProps', {})

  return {
    box: defaultTo(get(state, 'box.item'), defaultProps.box)
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ ...heroActions }, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeroesDrawer)
