// Libraries
import I18n from 'i18next'
import PropTypes from 'prop-types'
import { message, Tag } from 'antd'
import { connect } from 'react-redux'
import { get, defaultTo } from 'lodash'
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'

// Ressources
import { actions as heroActions } from 'Resources/HeroResource'

// Components
import { TableLayout } from 'Components'
import { Drawer } from './Components'

// Modal
import { ManageHero } from './Components/ManageModal'

class Heroes extends Component {
  static propTypes = {
    actions: PropTypes.object,
    heroes: PropTypes.array,
    isGathering: PropTypes.bool,
    tables: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    buttons: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    navigation: PropTypes.shape({
      navigate: PropTypes.func.isRequired,
      getParam: PropTypes.func.isRequired
    })
  }

  static defaultProps = {
    heroes: [],
    isGathering: false,
    isSubmitting: false,
    // buttons: {
    //  create: {
    //    label: I18n.t('common.create'),
    //    icon: 'plus'
    //  }
    // },
    tables: {
      heroes: {
        multiDelete: {},
        locale: {
          emptyText: I18n.t('pages.data.components.tabs.heroes.table.empty')
        },
        pagination: {
          position: 'both',
          showSizeChanger: true
        },
        rowKey: 'id',
        columns: {
          name: {
            title: I18n.t('pages.data.components.tabs.heroes.table.name'),
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) =>
              defaultTo(get(a, 'name'), '').localeCompare(
                defaultTo(get(b, 'name'), '')
              )
          },
          class: {
            title: I18n.t('pages.data.components.tabs.heroes.table.class'),
            render: heroes => {
              let color = 'orange'
              const classType = get(heroes, 'class')
              const element = get(heroes, 'element')

              switch (element) {
                case 'Light':
                  color = 'yellow'
                  break

                case 'Earth':
                  color = 'green'
                  break

                case 'Water':
                  color = 'geekblue'
                  break

                case 'Fire':
                  color = 'red'
                  break

                case 'Dark':
                  color = 'black'
                  break
              }
              return (
                <span className='hero'>
                  <span className='hero-name'>
                    {I18n.t(
                      `pages.data.components.tabs.heroes.table.${classType}`
                    )}
                  </span>
                  {element && (
                    <Tag color={color} className='hero-type'>
                      {I18n.t(
                        `pages.data.components.tabs.heroes.table.${element}`
                      )}
                    </Tag>
                  )}
                </span>
              )
            },
            sorter: (a, b) =>
              defaultTo(get(a, 'name'), '').localeCompare(
                defaultTo(get(b, 'name'), '')
              )
          },
          rarity: {
            title: I18n.t('pages.data.components.tabs.heroes.table.rarity'),
            render: heroes => {
              let rarity = get(heroes, 'rarity')

              switch (rarity) {
                case 1:
                  rarity = 'common'
                  break

                case 2:
                  rarity = 'rare'
                  break

                case 3:
                  rarity = 'epic'
                  break

                case 4:
                  rarity = 'legendary'
                  break
              }
              return (
                <span className='hero'>
                  <span className='hero-name'>
                    {I18n.t(
                      `pages.data.components.tabs.heroes.table.${rarity}`
                    )}
                  </span>
                </span>
              )
            },
            sorter: (a, b) =>
              defaultTo(get(a, 'rarity'), '').localeCompare(
                defaultTo(get(b, 'rarity'), '')
              )
          }
        }
      }
    }
  }

  constructor(props) {
    super(props)

    this.state = {
      openedModal: '',
      drawerOpened: false
    }
  }

  componentDidMount = () => {
    // Récupération des données des boîtiers
    this.fetchHeroesData()
  }

  /**
   * Récupération des données des boîtiers
   */
  fetchHeroesData = () => {
    this.props.actions.fetchHeroes().catch(() => {
      message.error(I18n.t('api.errors.hero.get'))
    })
  }

  /**
   * Demande de création d'un boîtier
   */
  askCreateHero = () => {
    this.setState({
      openedModal: 'createHero'
    })
  }

  /**
   * Fermeture de toutes les modales
   */
  handleCloseModal = () => {
    this.setState({ openedModal: '' })
  }

  /**
   * Fermeture de la modale après validation
   */
  handleHeroCreated = () => {
    this.setState({ openedModal: '' })
  }

  /**
   * Clic sur une ligne de la liste globale
   */
  handleHeroClick = hero => {
    this.setState({ hero, drawerOpened: true })
  }

  /**
   * Fermeture du drawer
   */
  handleCloseDrawer = hero => {
    this.setState({ hero: {}, drawerOpened: false })
  }

  render() {
    const { tables, buttons, isGathering, heroes } = this.props
    const { drawerOpened, hero, openedModal } = this.state

    // Ajout des données au tableau
    tables.heroes.rows = heroes

    // Ajout des actions au clic sur un ligne du tableau
    tables.heroes.onRow = (hero, id) => ({
      onClick: () => this.handleHeroClick(hero, id)
    })

    // Ajout du multiple archive sur le tableau
    tables.heroes.multiDelete.onClick = this.askArchiveHeroes
    tables.heroes.multiDelete.title = count =>
      I18n.t('components.tableLayout.archiveMany', { count })

    // Ajout des actions des boutons
    // buttons.create.action = this.askCreateHero

    return (
      <main className='screen hero'>
        {/* Affichage des boîtiers */}

        <TableLayout
          elementName='hero'
          elementKind='male'
          elementConsonant
          buttons={buttons}
          loading={isGathering}
          tables={tables}
        />

        {/* Creation d'un boîtier */}
        <ManageHero
          visible={openedModal === 'createHero'}
          onCancel={this.handleCloseModal}
          onOk={this.handleHeroCreated}
        />

        {/* Affichage d'un boîtier */}
        <Drawer
          onClose={this.handleCloseDrawer}
          id={get(hero, 'id')}
          visible={drawerOpened}
        />
      </main>
    )
  }
}

const mapStateToProps = state => {
  const defaultProps = get(Heroes, 'defaultProps', {})

  return {
    heroes: defaultTo(get(state, 'heroes.items'), defaultProps.heroes)
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ ...heroActions }, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Heroes)
