// Libraries
import I18n from 'i18next'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { Form, Input, message, Select } from 'antd'
import { get, has, map, defaultTo, cloneDeep, groupBy, isNil } from 'lodash'

// Components
import { Container } from 'Components/Modals'

// Resources
import { actions as heroActions } from 'Resources/HeroResource'

// Styles
import './Styles/Modal.less'

// Helper
import { createFormFields } from 'Helpers'

const { Item: FormItem } = Form
const { Option, OptGroup } = Select

class ManageHeroModal extends Component {
  static propTypes = {
    // Contrôle d'état de la modale
    okText: PropTypes.string,
    title: PropTypes.string,
    visible: PropTypes.bool,

    // Contenu de la modale
    hero: PropTypes.object,
    form: PropTypes.object,
    heroAreas: PropTypes.array,

    // Appels API
    actions: PropTypes.object,

    // Fonctions de callback
    onOk: PropTypes.func,
    onCancel: PropTypes.func
  }

  static defaultProps = {
    visible: false,
    formData: {},
    onOk: () => {},
    onCancel: () => {},
    hero: {},
    heroAreas: [],
    okText: I18n.t('pages.heroes.components.modal.createHero.okText'),
    title: I18n.t('pages.heroes.components.modal.createHero.title')
  }

  constructor(props) {
    super(props)

    // États initiaux
    this.state = {
      visible: get(props, 'visible'),
      loading: false
    }

    // Modale ouverte en mode création ?
    this.isCreating = !has(props.hero, 'id')
  }
  componentDidMount = () => {
    // this.fetchHeroArea()
  }

  componentDidUpdate(prevProps) {
    const { hero, visible } = this.props

    if (prevProps.hero !== hero) {
      // Modale ouverte en mode création ?
      this.isCreating = !has(hero, 'id')
    }

    if (prevProps.visible !== visible) {
      // Changement de visibilité de la modale
      this.changeModalVisibility(visible)
    }
  }

  /**
   * Changement de visibilité de la modale
   */
  changeModalVisibility = visible => {
    this.setState({ visible })
  }

  handleCancel = e => {
    const { onCancel, form } = this.props

    this.setState(
      {
        visible: false
      },
      () => {
        onCancel(e)
        form.resetFields()
      }
    )
  }

  createHero = hero => {
    const { form, onOk } = this.props

    this.props.actions
      .createHero(hero)
      .then(({ body: hero }) => {
        message.success(I18n.t('api.success.hero.create'))
        onOk(hero)

        form.resetFields()
      })
      .catch(err => {
        // Problème lors de la création
        if (err.status === 400) {
          message.error(I18n.t('api.errors.hero.create'))
        }
      })
      .finally(() => {
        this.setState({
          loading: false
        })
      })
  }

  updateHero = hero => {
    const { form, onOk } = this.props

    this.props.actions
      .updateHero({ ...hero, heroArea: defaultTo(get(hero, 'heroArea'), null) })
      .then(({ body: hero }) => {
        message.success(I18n.t('api.success.hero.update'))

        // Callbacks
        onOk(hero)
        form.resetFields()

        // Récupération des hero libres
        this.fetchHeroArea()
      })
      .catch(err => {
        // Problème lors de la création
        if (err) {
          message.error(I18n.t('api.errors.hero.update'))
        }
      })
      .finally(() => {
        this.setState({
          loading: false
        })
      })
  }

  fetchHeroArea = () => {
    this.props.actions.fetchHeroAreas().catch(() => {
      message.error(I18n.t('api.errors.hero.getHeroArea'))
    })
  }

  handleSubmit = e => {
    e.preventDefault()
    const {
      form,
      hero: { id }
    } = this.props

    // Validation du formulaire
    form.validateFieldsAndScroll((error, hero) => {
      if (!error) {
        this.setState({ loading: true }, () => {
          this.isCreating
            ? this.createHero({ ...hero })
            : this.updateHero({ id, ...hero })
        })
      }
    })
  }

  render() {
    const { visible = false, loading = false } = this.state
    const { form, title, okText, heroAreas } = this.props
    const { getFieldDecorator } = form

    // Changement de titre (création ou modification)
    const updatedTitle = !this.isCreating
      ? I18n.t('pages.heroes.components.modal.updateHero.title')
      : title
    const updatedOkText = !this.isCreating
      ? I18n.t('pages.heroes.components.modal.updateHero.okText')
      : okText

    return (
      <Container
        title={updatedTitle}
        visible={visible}
        loading={loading}
        onOk={this.handleSubmit}
        okText={updatedOkText}
        onCancel={this.handleCancel}
        className={'modal createHero'}
      >
        <Form onSubmit={this.handleSubmit}>
          {/* Numéro de série */}
          <FormItem label={I18n.t(`fields.serialNumber.title`)}>
            {getFieldDecorator('serialNumber', {
              rules: [
                {
                  required: true,
                  message: I18n.t(`fields.serialNumber.requiredMessage`)
                }
              ]
            })(
              <Input placeholder={I18n.t(`fields.serialNumber.placeholder`)} />
            )}
          </FormItem>

          {/* zone associée que si update */}

          {!this.isCreating && (
            <FormItem label={I18n.t(`fields.heroArea.title`)}>
              {getFieldDecorator('heroArea', {})(
                <Select
                  allowClear
                  placeholder={I18n.t(`fields.heroArea.placeholder`)}
                >
                  {map(groupBy(heroAreas, 'type'), (areas, type) => (
                    <OptGroup
                      key={type}
                      label={I18n.t(
                        `pages.heroes.table.columns.associated.type.${type}`
                      )}
                    >
                      {map(areas, area => (
                        <Option key={area.id} value={area.id}>
                          {area.name}
                        </Option>
                      ))}
                    </OptGroup>
                  ))}
                </Select>
              )}
            </FormItem>
          )}

          {/* identifiant matériel Lora à masquer si pas Ubiadmin */}
          <FormItem label={I18n.t(`fields.devEUI.title`)}>
            {getFieldDecorator('devEUI', {
              rules: [
                {
                  required: true,
                  message: I18n.t(`fields.devEUI.requiredMessage`)
                }
              ]
            })(<Input placeholder={I18n.t(`fields.devEUI.placeholder`)} />)}
          </FormItem>
        </Form>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  const defaultProps = get(ManageHeroModal, 'defaultProps', {})

  let availableHeroAreas = []

  // Ajout de la zone hero actuellement affectée
  if (!isNil(get(state, 'hero.item.heroArea'))) {
    availableHeroAreas.push(get(state, 'hero.item.heroArea'))
  }

  // Ajout des zones hero disponibles
  availableHeroAreas = [
    ...availableHeroAreas,
    ...defaultTo(get(state, 'heroArea.items'), defaultProps.heroAreas)
  ]

  return {
    heroAreas: availableHeroAreas
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      ...heroActions
    },
    dispatch
  )
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  Form.create({
    mapPropsToFields(props) {
      const defaultProps = get(ManageHeroModal, 'defaultProps', {})
      const hero = cloneDeep(defaultTo(get(props, 'hero'), defaultProps.hero))
      hero.heroArea = get(hero, 'heroArea.id')

      const isCreating = !has(props.hero, 'id')

      if (!isCreating) {
        hero.linkedProjects = map(hero.linkedProjects, 'id')
      }

      return createFormFields(Form, hero)
    }
  })(ManageHeroModal)
)
