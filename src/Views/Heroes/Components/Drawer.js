// Libraries
import I18n from 'i18next'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { get, defaultTo, isNil } from 'lodash'
import { Drawer, Button, message, Modal } from 'antd'

// Styles
import './Styles/HeroesDrawer.less'

// Ressources
import { actions as heroActions } from 'Resources/HeroResource'

// Modal
import { ManageHero } from './ManageModal'

const { confirm } = Modal

class HeroesDrawer extends Component {
  static propTypes = {
    // Données de la vue
    id: PropTypes.number,
    hero: PropTypes.object,

    // Contrôle de l'affichage
    visible: PropTypes.bool,

    // Fonctions
    onClose: PropTypes.func,
    actions: PropTypes.object
  }

  static defaultProps = {
    // Données de la vue
    hero: {}
  }

  constructor(props) {
    super(props)

    // États initiaux
    this.state = {
      openedModal: '',
      hero: {},
      visible: get(props, 'visible')
    }
  }

  componentDidUpdate = oldProps => {
    const { id, visible } = this.props

    // Changement de cadenas ouvert depuis la liste
    if (!isNil(id) && oldProps.id !== id) {
      this.getHeroData()
    }

    // Changement de visibilité du panneau
    if (oldProps.visible !== visible) {
      this.changeModalVisibility(visible)
    }
  }

  /**
   * Changement de visibilité du panneau
   */
  changeModalVisibility = visible => {
    this.setState({ visible })
  }

  /**
   * Fermeture du panneau
   */
  handleClose = () => {
    const { onClose } = this.props

    this.setState({ visible: false }, () => setTimeout(onClose, 300))
  }

  /**
   * Récupération des données du cadenas
   */
  getHeroData = () => {
    const { id } = this.props

    this.props.actions
      .getHero(id)
      .catch(() => message.error(I18n.t('api.errors.hero.get')))
  }

  /**
   * Rendu d'une ligne de détails formatée (ex: Label: Valeur)
   */
  renderDescriptionItem = (name, value) => (
    <p className='hero-details-description-item'>
      <span className='hero-details-description-item-name'>
        {I18n.t(`pages.heroes.components.drawer.description.${name}`)}
      </span>
      <span className='hero-details-description-item-value'>
        {defaultTo(value, get(this.props, `hero.${name}`))}
      </span>
    </p>
  )

  /**
   * Mise en archive du boîtier
   */
  archiveHero = () => {
    const { hero } = this.props

    confirm({
      title: I18n.t('pages.heroes.components.modal.archiveHero.title'),
      content: I18n.t('pages.heroes.components.modal.archiveHero.content'),
      okText: I18n.t('common.archive'),
      cancelText: I18n.t('common.cancel'),
      okType: 'danger',
      onOk: () => {
        this.props.actions
          .archiveHero({
            id: get(hero, 'id')
          })
          .then(() => {
            message.success(I18n.t(`api.success.hero.archive`))
          })
          .catch(() => {
            message.error(I18n.t(`api.errors.hero.archive`))
          })
      }
    })
  }

  /**
   * Ouverture de la modale de mise à jour
   */
  askUpdateHero = () => {
    this.setState({ openedModal: 'updateHero' })
  }

  /**
   * Fermeture de toutes les modales
   */
  handleCloseModal = () => {
    this.setState({ openedModal: '' })
  }

  /**
   * Fermeture de la modale après validation
   */
  handleHeroUpdate = () => {
    this.setState({ openedModal: '' })
  }

  render() {
    const { hero } = this.props
    const { visible, openedModal } = this.state

    return (
      <Drawer
        width={640}
        placement='right'
        onClose={this.handleClose}
        visible={visible}
        className='hero-details'
      >
        {/* Titre */}
        <h2>{I18n.t('pages.heroes.components.drawer.title')}</h2>

        {/* Informations principales */}
        <main className='hero-details-description'>
          {/* Informations détaillés  */}
          <section className='hero-details-description-group'>
            <h3>{I18n.t('pages.heroes.components.drawer.sections.details')}</h3>

            {/* Numéro de série */}
            {this.renderDescriptionItem('serialNumber')}

            {/* Lieu associé au boîtier */}
            {this.renderDescriptionItem('heroArea.name')}

            {/* identifiant Lora */}
            {this.renderDescriptionItem('devEUI')}
          </section>
        </main>

        {/* Boutons d'actions */}
        <footer className='hero-details-footer'>
          <Button onClick={this.askUpdateHero} type='primary'>
            {I18n.t('common.update')}
          </Button>

          <div className='buttons'>
            <Button onClick={this.archiveHero} type='danger'>
              {I18n.t('common.archive')}
            </Button>
          </div>
        </footer>

        {/* Modale de mise à jour */}
        <ManageHero
          visible={openedModal === 'updateHero'}
          onCancel={this.handleCloseModal}
          onOk={this.handleHeroUpdate}
          hero={hero}
        />
      </Drawer>
    )
  }
}

const mapStateToProps = state => {
  const defaultProps = get(HeroesDrawer, 'defaultProps', {})

  return {
    hero: defaultTo(get(state, 'hero.item'), defaultProps.hero)
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ ...heroActions }, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeroesDrawer)
