// Libraries
import I18n from 'i18next'
import { Tabs } from 'antd'
import { connect } from 'react-redux'
import React, { Component } from 'react'

// Tabs
import { Artefacts } from './../Artefacts'
import { Heroes } from './../Heroes'

// Styles
import './Styles/UserDataScreen.less'

const { TabPane } = Tabs

class UserData extends Component {
  static propTypes = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {
    return (
      <main className='screen hero'>
        {/* Affichage des boîtiers */}

        {/* Tabs */}
        <Tabs defaultActiveKey='heroes' type='card'>
          {/* Heroes */}
          <TabPane
            tab={I18n.t('pages.data.components.tabs.heroes.title')}
            key='heroes'
          >
            <Heroes />
          </TabPane>

          {/* Artefacts */}
          <TabPane
            tab={I18n.t('pages.data.components.tabs.artefacts.title')}
            key='artefacts'
          >
            <Artefacts />
          </TabPane>
        </Tabs>
      </main>
    )
  }
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserData)
