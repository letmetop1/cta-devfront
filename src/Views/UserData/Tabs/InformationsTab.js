import React, { Component } from 'react'

// Libraries
import I18n from 'i18next'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { get, defaultTo } from 'lodash'
import { Form, Input, Icon, Card, Button, message } from 'antd'

// Helpers
import { createFormFields } from 'Helpers'

// Resources
import { actions as profileActions } from 'Resources/ProfileResource'

const { Item: FormItem } = Form

class InformationsTab extends Component {
  static propTypes = {
    form: PropTypes.object,
    profile: PropTypes.object,
    actions: PropTypes.object,
    isGathering: PropTypes.bool,
    isSubmitting: PropTypes.bool
  }

  static defaultProps = {
    profile: {},
    isGathering: false,
    isSubmitting: false
  }

  constructor(props) {
    super(props)

    this.state = {}
  }

  componentDidMount = () => {
    // Récupération du profil
    this._getProfileData()
  }

  /**
   * Récupération du profil
   */
  _getProfileData = () => {
    this.props.actions.getProfile().catch(() => {
      message.error(I18n.t('api.errors.profile.get'))
    })
  }

  /**
   * Envoi du formulaire
   */
  _handleSubmit = e => {
    e.preventDefault()

    const { form, profile: originalProfile } = this.props

    // Validation du formulaire
    form.validateFieldsAndScroll((error, profile) => {
      profile = { ...originalProfile, ...profile }

      if (!error) {
        this.props.actions
          .updateProfile(profile)
          .then(() => {
            message.success(I18n.t('api.success.profile.update'))
          })
          .catch(err => {
            // Création impossible, profileCode ou email déjà utilisé
            if (err.status === 400 && get(err, 'body.key') === 'CONFLICT') {
              err.body.field === 'email' &&
                message.error(
                  I18n.t('api.errors.profile.update.conflict.email')
                )
              err.body.field === 'profileCode' &&
                message.error(
                  I18n.t('api.errors.profile.update.conflict.profileCode')
                )
            } else {
              message.error(I18n.t('api.errors.profile.update.default'))
            }
          })
      }
    })
  }

  _renderForm = () => {
    const { form, isSubmitting } = this.props
    const { getFieldDecorator } = form

    return (
      <Form onSubmit={this._handleSubmit}>
        {/* Prénom */}
        <FormItem label={I18n.t('fields.firstName.title')}>
          {getFieldDecorator('firstName', {
            rules: [
              {
                required: true,
                message: I18n.t('fields.firstName.requiredMessage')
              }
            ]
          })(
            <Input
              prefix={<Icon type={I18n.t('fields.firstName.icon')} />}
              placeholder={I18n.t('fields.firstName.placeholder')}
            />
          )}
        </FormItem>

        {/* Nom */}
        <FormItem label={I18n.t('fields.lastName.title')}>
          {getFieldDecorator('lastName', {
            rules: [
              {
                required: true,
                message: I18n.t('fields.lastName.requiredMessage')
              }
            ]
          })(
            <Input
              prefix={<Icon type={I18n.t('fields.lastName.icon')} />}
              placeholder={I18n.t('fields.lastName.placeholder')}
            />
          )}
        </FormItem>

        {/* Email */}
        <FormItem label={I18n.t('fields.email.title')}>
          {getFieldDecorator('email', {
            rules: [
              {
                required: true,
                message: I18n.t('fields.email.requiredMessage')
              }
            ]
          })(
            <Input
              prefix={<Icon type={I18n.t('fields.email.icon')} />}
              placeholder={I18n.t('fields.email.placeholder')}
            />
          )}
        </FormItem>

        <FormItem>
          <Button
            type='primary'
            loading={isSubmitting}
            onClick={this._handleSubmit}
          >
            {I18n.t('common.save')}
          </Button>
        </FormItem>
      </Form>
    )
  }

  render() {
    const { isGathering } = this.props

    return (
      <Card loading={isGathering} className='tab informations'>
        {this._renderForm()}
      </Card>
    )
  }
}

const mapStateToProps = state => {
  const defaultProps = get(InformationsTab, 'defaultProps', {})

  return {
    profile: defaultTo(get(state, 'profile.item'), defaultProps.profile),
    isGathering: defaultTo(
      get(state, 'profile.isFetching'),
      defaultProps.isGathering
    ),
    isSubmitting: defaultTo(
      get(state, 'profile.isUpdating'),
      defaultProps.isSubmitting
    )
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ ...profileActions }, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  Form.create({
    mapPropsToFields(props) {
      const defaultProps = get(InformationsTab, 'defaultProps', {})
      const profile = defaultTo(get(props, 'profile'), defaultProps.profile)

      return createFormFields(Form, profile)
    }
  })(InformationsTab)
)
