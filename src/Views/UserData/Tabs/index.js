import Informations from './InformationsTab'
import Heroes from './HeroesTab'

export default Heroes
export { Heroes, Informations }
