// Libraries
// import I18n from 'i18next'
import { connect } from 'react-redux'
import React, { Component } from 'react'

// Styles
import './Styles/HomeScreen.less'

class Home extends Component {
  static propTypes = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {
    return (
      <main className='screen box'>
        {/* Affichage des la Page d'accueil */}

        <span>
          <p>Salut</p>
        </span>
      </main>
    )
  }
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
