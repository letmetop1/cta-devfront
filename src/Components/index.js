// Components
import TableLayout from './TableLayout'
import Modals from './Modals'
import Anchor from './Anchor'
import LanguageSwitcher from './LanguageSwitcher'
import StrengthIndicator from './StrengthIndicator'

export default TableLayout
export { TableLayout, Modals, Anchor, LanguageSwitcher, StrengthIndicator }
